# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/26 18:45:59 by dtitenko          #+#    #+#              #
#    Updated: 2017/04/09 23:13:33 by dtitenko         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol

RM = /bin/rm
MKDIR = /bin/mkdir
PRINTF = /usr/bin/printf
ECHO = /bin/echo

LIBNAME = ft
LIBDIR = ./libft
LIBINCLUDEFOLDERS = ./libft/includes

PRINTFNAME = ftprintf
PRINTFDIR = ./ft_printf
PRINTFINCUDES = ./ft_printf/includes

MLXNAME = mlx
MLXDIR = ./minilibx_x11
MLXINCLUDES = ./minilibx_x11

X11INCLUDES = /opt/X11/include

INCLUDES = ./include

SOURCES_FOLDER = src/
OBJECTS_FOLDER = obj/

SOURCES = main.c \
		image.c \
		modifiers_hndlrs.c \
		mlx_init.c \
		error.c \
		mouse_hooks.c \
		other_hooks.c \
		kb_press_hooks.c \
		scene.c \
		argv_parsing.c \
		usage.c \
		ft_init_fractal.c \
		ft_zoom_fractal.c \
		ft_move_fractal.c \
		ft_init_kernel.c \
		kb_release_hooks.c \
		help.c \
		opencl/cl_kernal_from_file.c \
		opencl/cl_release_all.c \
		opencl/cl_init.c

OBJECTS = $(SOURCES:.c=.o)
OBJECTS := $(addprefix $(OBJECTS_FOLDER), $(OBJECTS))
SOURCES := $(addprefix $(SOURCES_FOLDER), $(SOURCES))

CC = clang
AR = ar
CFLAGS = -Wall -Werror -Wextra


# Colors

NO_COLOR =		\033[0;00m
OK_COLOR =		\033[38;5;02m
ERROR_COLOR =	\033[38;5;01m
WARN_COLOR =	\033[38;5;03m
SILENT_COLOR =	\033[38;5;04m

IFLAGS = -I. -I$(INCLUDES) -I$(X11INCLUDES) \
		-I$(PRINTFINCUDES) -I$(LIBINCLUDEFOLDERS)  -I$(MLXINCLUDES)

LFLAGS = -L$(PRINTFDIR) -l$(PRINTFNAME) \
		-L$(LIBDIR) -l$(LIBNAME) \
		-L/opt/X11/lib -lX11 -lXext

# Basic Rules

.PHONY: all re clean fclean tests

all: $(NAME)

$(NAME): $(OBJECTS) $(MLXNAME) $(LIBNAME) $(PRINTFNAME)
	@$(PRINTF) "$(SILENT_COLOR)./$(NAME) binary$(NO_COLOR)"
	@$(CC) $(CFLAGS) -framework OpenCL $(IFLAGS) $(LFLAGS) \
	-o $(NAME) minilibx_x11/libmlx.a $(OBJECTS)
	@$(PRINTF)		"\t[$(OK_COLOR)✓$(NO_COLOR)]$(NO_COLOR)\n"

$(OBJECTS_FOLDER)%.o: $(SOURCES_FOLDER)%.c
	@mkdir -p $(OBJECTS_FOLDER)
	@mkdir -p $(OBJECTS_FOLDER)/opencl
	@$(CC) $(CFLAGS) $(IFLAGS) -o $@ -c $<
	@$(PRINTF) "$(OK_COLOR)✓ $(NO_COLOR)$<\n"


$(MLXNAME):
	make -C $(MLXDIR) all

$(LIBNAME):
	make -C $(LIBDIR) all

$(PRINTFNAME):
	make -C $(PRINTFDIR) all


clean:
	make -C $(MLXDIR) clean
	make -C $(LIBDIR) clean
	make -C $(PRINTFDIR) clean
	rm -f $(OBJECTS)
	rm -rf $(OBJECTS_FOLDER)
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Objects libftprintf$(NO_COLOR)\n"

fclean: clean
	make -C $(MLXDIR) clean
	make -C $(LIBDIR) fclean
	make -C $(PRINTFDIR) fclean
	rm -f ./basic_test
	rm -f $(NAME)
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Library libftprintf$(NO_COLOR)\n"

re: fclean all