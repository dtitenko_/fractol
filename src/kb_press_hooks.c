/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   kb_press_hooks.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/24 19:29:05 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/24 19:29:09 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf/includes/ft_printf.h>
#include "fractol.h"
#include <limits.h>
#define CF scene.fractal
#define IS_SHIFTMOD (mlx->keymodif & SHIFTMOD)
#define INC_CF_COLOR (IS_SHIFTMOD ? CF->base_color2++ : CF->base_color1++)
#define DEINC_CF_COLOR (IS_SHIFTMOD ? CF->base_color2-- : CF->base_color1--)

void	color_hooks(int keycode, t_scene scene, t_mlx *mlx)
{
	if (XK_p == keycode)
		CF->psychomod = (CF->psychomod + 1) % 2;
	else if (XK_period == keycode)
		INC_CF_COLOR;
	else if (XK_comma == keycode)
		DEINC_CF_COLOR;
	else if (XK_slash == keycode)
		ft_reset_color(CF);
}

void	param_hooks(int keycode, t_scene scene)
{
	if (XK_f == keycode)
		CF->fixed = (scene.fractal->fixed + 1) % 2;
	else if (XK_r == keycode)
		ft_reset_bounds(scene.fractal);
	else if (XK_KP_Add == keycode)
		CF->maxiter += CF->maxiter + 5 < INT_MAX ? 5 : 0;
	else if (XK_KP_Subtract == keycode)
		CF->maxiter -= CF->maxiter - 5 > 0 ? 5 : 0;
	else if (XK_equal == keycode)
		CF->pow += CF->pow <= INT_MAX ? 1 : 0;
	else if (XK_minus == keycode)
		CF->pow = CF->pow < 3 ? 2 : CF->pow - 1;
}

void	move_hooks(int keycode, t_scene scene)
{
	(XK_Right == keycode) ? ft_move_d(CF, 5, 0) : 0;
	(XK_Left == keycode) ? ft_move_d(CF, -5, 0) : 0;
	(XK_Up == keycode) ? ft_move_d(CF, 0, -5) : 0;
	(XK_Down == keycode) ? ft_move_d(CF, 0, 5) : 0;
	(XK_Prior == keycode) ? ft_set_zoom(CF, 1.03, WINW / 2, WINH / 2) : 0;
	(XK_Next == keycode) ? ft_set_zoom(CF, 0.97, WINW / 2, WINH / 2) : 0;
}

int		keypress_hook(int keycode, t_mlx *mlx)
{
	t_scene scene;

	scene = mlx->currscene;
	if (!(scene.winp) || scene.winp->window != mlx->curr->window)
		scene = find_scene(mlx, mlx->winp);
	if (XK_Escape == keycode)
		exit_hook(mlx);
	param_hooks(keycode, scene);
	color_hooks(keycode, scene, mlx);
	move_hooks(keycode, scene);
	update_scene(scene, mlx);
	modifiers_keypress(keycode, mlx);
	return (0);
}
