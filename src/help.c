/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/24 20:36:01 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/24 20:36:01 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

static void	put_string(t_mlx *mlx, int x, int y, char *str)
{
	int i;

	i = -1;
	while (++i < mlx->numscenes)
		mlx_string_put(mlx->mlxp, mlx->scenes[i].winp, x, y, 0xFFFFFF, str);
}

void		draw_usage(t_mlx *mlx)
{
	if (!(mlx->flags & SHOWHELP))
		return ;
	put_string(mlx, 10, 15, "Usage:");
	put_string(mlx, 10, 45, "F1 - Show/Hide this usage;");
	put_string(mlx, 10, 75, "+/-(Numpad) - inc/dec iterations");
	put_string(mlx, 10, 90, "+/-(Main KB) - inc/dec power of fractal");
	put_string(mlx, 10, 105, "arrows - translate fractal;");
	put_string(mlx, 10, 120, "p - psychomode;");
	put_string(mlx, 10, 135, "</> - inc color");
	put_string(mlx, 10, 150, "/ - reset color color");
	put_string(mlx, 10, 165, "r - reset position");
	put_string(mlx, 10, 180, "f - static mode");
	put_string(mlx, 10, 195, "PgUp/PgDown - zoom in/zoom out to "
		"center of window");
}
