/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:40 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:40 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	main(int argc, char **argv)
{
	t_mlx	*mlx;
	int		stop;
	t_ftype	*types;

	stop = 0;
	mlx = NULL;
	types = NULL;
	if (argc > 1)
	{
		if (parse_argv(argc, argv, &stop))
			return (0);
		if (!(types = ft_parse_fractals(argc, argv, &stop, &mlx)))
			ft_die(argc, argv, ENOMEM, &mlx);
	}
	else
		ft_die(argc, argv, ENODEV, &mlx);
	mlx = ft_init_mlx(stop, types);
	update(mlx);
	mlx_loop(mlx->mlxp);
	ft_mlxdel(&mlx);
	return (0);
}
