/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:13:24 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:13:25 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_SCENE_H
# define FRACTOL_SCENE_H

# ifndef MLX_INT_H
#  define MLX_INT_H
#  include "mlx_int.h"
# endif

# include "fractol.h"
# define WINW 720
# define WINH 640

t_scene	*map_scenes(t_mlx *mlx, t_scene (*func)());
t_scene	clear_scene(t_scene scene, t_mlx *mlx);
t_scene	draw_scene(t_scene scene, t_mlx *mlx);
t_scene	update_scene(t_scene scene, t_mlx *mlx);
t_scene	find_scene(t_mlx *mlx, t_win_list *win);

#endif
