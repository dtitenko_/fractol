/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_math.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/06 23:27:24 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:18:02 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MATH_H
# define FT_MATH_H

# define ABS(x) ((x < 0) ? -x : x)

unsigned long long int	ft_llabs(long long int nb);
unsigned long int		ft_labs(long int nb);
unsigned int			ft_abs(int nb);
double					ft_fabs(double nb);

long long int			ft_llpow(long long int base, unsigned int exp);
long int				ft_lpow(long int base, unsigned int exp);
int						ft_pow(int base, unsigned int exp);

long double				ft_powf(double base, int exp);
long double				ft_roundl(long double num, unsigned int dec);

#endif
