/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_kernel.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:21 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:22 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"
#include "libft/includes/libft.h"

#define MAX_ITER	0
#define COLOR1		1
#define COLOR2		2
#define MINRE		3
#define MAXRE		4
#define MINIM		5
#define MAXIM		6
#define FACTOR_X	7
#define FACTOR_Y	8
#define SHIFT_X		9
#define SHIFT_Y		10
#define PSYCHO		11
#define POWER		12

#define ARGS_SIZE	13

double	*ft_get_args(t_fractal *p_fract)
{
	double	*args;

	args = ft_memalloc((size_t)ARGS_SIZE * sizeof(double));
	if (!args)
		return (NULL);
	args[MAX_ITER] = p_fract->maxiter;
	args[COLOR1] = p_fract->base_color1;
	args[COLOR2] = p_fract->base_color2;
	args[MINRE] = p_fract->min.re;
	args[MAXRE] = p_fract->max.re;
	args[MINIM] = p_fract->min.im;
	args[MAXIM] = p_fract->max.im;
	args[FACTOR_X] = p_fract->factor.re;
	args[FACTOR_Y] = p_fract->factor.im;
	args[SHIFT_X] = p_fract->shift.re;
	args[SHIFT_Y] = p_fract->shift.im;
	args[PSYCHO] = p_fract->psychomod;
	args[POWER] = p_fract->pow;
	return (args);
}

void	ft_exec_kernel(t_scene *p_scene)
{
	t_ocl	*p_ocl;
	size_t	device_work_offset[2];
	size_t	device_work_size[2];

	p_ocl = (*p_scene).p_ocl;
	OCL_BUFS[0].mem = p_scene->imgp->data;
	if (!(OCL_BUFS[1].mem = ft_get_args(p_scene->fractal)))
		return ;
	device_work_offset[0] = 0;
	device_work_offset[1] = 0;
	device_work_size[0] = WINW;
	device_work_size[1] = WINH;
	clEnqueueWriteBuffer(OCL_CMD_QUEUES[0], OCL_BUFS[0].buf, CL_FALSE, 0,
						OCL_BUFS[0].buf_size, OCL_BUFS[0].mem, 0, NULL, NULL);
	clEnqueueWriteBuffer(OCL_CMD_QUEUES[0], OCL_BUFS[1].buf, CL_FALSE, 0,
						OCL_BUFS[1].buf_size, OCL_BUFS[1].mem, 0, NULL, NULL);
	OCL_ERR = clEnqueueNDRangeKernel(OCL_CMD_QUEUES[0], OCL_KRNL, 2,
									device_work_offset, device_work_size,
									NULL, 0, NULL, NULL);
	clEnqueueReadBuffer(OCL_CMD_QUEUES[0], OCL_BUFS[0].buf, CL_TRUE, 0,
						OCL_BUFS[0].buf_size, OCL_BUFS[0].mem, 0, NULL, NULL);
	clFinish(OCL_CMD_QUEUES[0]);
	ft_memdel(&(OCL_BUFS[1].mem));
}

t_ocl	*ft_init_kernel(char *path, char *name)
{
	t_ocl	*p_ocl;
	size_t	buf_sizes[2];

	buf_sizes[0] = WINH * WINW * sizeof(int);
	buf_sizes[1] = ARGS_SIZE * sizeof(double);
	if (!(p_ocl = ftocl_init_ocl(1, buf_sizes, 2)))
		return (NULL);
	OCL_BUFS[0].buf = clCreateBuffer(OCL_CTX, CL_MEM_READ_WRITE,
									buf_sizes[0], NULL, &OCL_ERR);
	OCL_BUFS[1].buf = clCreateBuffer(OCL_CTX, CL_MEM_WRITE_ONLY,
									buf_sizes[1], NULL, &OCL_ERR);
	OCL_KRNL = ftocl_get_kernel_from_file(p_ocl, path, name);
	OCL_ERR = clSetKernelArg(OCL_KRNL, 0, sizeof(cl_mem), &(OCL_BUFS[0].buf));
	OCL_ERR |= clSetKernelArg(OCL_KRNL, 1, sizeof(cl_mem), &(OCL_BUFS[1].buf));
	return (p_ocl);
}
