/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_fractal.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:24 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:24 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_complex	ft_recount_coordinates(t_fractal *fractal, int x, int y)
{
	t_complex d;

	d.re = (double)x * fractal->factor.re + fractal->min.re;
	d.im = (double)y * fractal->factor.im + fractal->min.im;
	return (d);
}

void		ft_move2center(t_fractal *fractal, int x, int y)
{
	t_complex d;
	t_complex c;

	c = ft_recount_coordinates(fractal, WINW / 2, WINH / 2);
	d = ft_recount_coordinates(fractal, x, y);
	d.im = c.im - d.im;
	d.re = c.re - d.re;
	fractal->min.re -= d.re;
	fractal->max.re -= d.re;
	fractal->min.im -= d.im;
	fractal->max.im -= d.im;
}

void		ft_move_d(t_fractal *fractal, int dx, int dy)
{
	ft_move2center(fractal, WINW / 2 + dx, WINH / 2 + dy);
}
