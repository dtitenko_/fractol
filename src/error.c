/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:14 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:15 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "ft_printf/includes/ft_printf.h"

void	ft_die(int argc, char **argv, int code, t_mlx **mlx)
{
	ft_mlxdel(mlx);
	if (code == ENODEV)
	{
		ft_printf("{r}%s : invalid number of arguments : %d{eoc}\n",
					argv[0], argc - 1);
		ft_usage(argv[0]);
	}
	if (code == ENOENT && argc > 1)
		ft_printf("{r}%s : cannot access %s : no such file or directory{eoc}\n",
					argv[0], argv[1]);
	if (code == EIO && argc > 1)
		ft_printf("{r}%s : %s : invalid file{eoc}\n", argv[0], argv[1]);
	if (code == ENOMEM)
		ft_printf("{r}%s : error : cannot allocate memory{eoc}\n", argv[0]);
	exit(code);
}
