/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_fractal.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:18 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:18 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/ft_ocl.h>
#include <libft/includes/libft.h>
#include <include/fractol.h>
#include "fractal.h"

char		*get_path(t_ftype ftype)
{
	if (ftype == mandelbrot)
		return ("src/kernels/mandelbrot.cl");
	if (ftype == julia)
		return ("src/kernels/julia.cl");
	return ("src/kernels/bship.cl");
}

void		ft_reset_bounds(t_fractal *fractal)
{
	fractal->min.re = -2.2;
	fractal->max.re = 2;
	fractal->min.im = -2.2;
	fractal->max.im = 2.2;
	fractal->zoom = 1.0;
	ft_count_factor(fractal);
}

void		ft_reset_color(t_fractal *fractal)
{
	fractal->base_color1 = 3;
	fractal->base_color2 = 0;
}

t_fractal	*ft_init_fractal(t_ftype ftype, t_scene *scene)
{
	t_fractal	*fractal;

	if (!(fractal = ft_memalloc(sizeof(t_fractal))))
		return (NULL);
	fractal->maxiter = 256;
	fractal->type = ftype;
	fractal->func = &ft_exec_kernel;
	fractal->fixed = 0;
	ft_reset_color(fractal);
	fractal->psychomod = 0;
	fractal->pow = 2;
	ft_reset_bounds(fractal);
	scene->p_ocl = ft_init_kernel(get_path(ftype), "render");
	return (fractal);
}
