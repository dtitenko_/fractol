/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractal.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:13:07 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:13:07 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_FRACTAL_H
# define FRACTOL_FRACTAL_H

# include "structs.h"

# define INIT_MAX_ITERS 256

t_fractal	*ft_init_fractal(t_ftype ftype, t_scene *scene);
t_complex	ft_recount_coordinates(t_fractal *fractal, int x, int y);

double		interpolate(double start, double end, double interpolation);

void		ft_count_factor(t_fractal *fractal);
void		ft_count_bounds(t_fractal *fractal, t_complex d);
void		ft_set_zoom(t_fractal *fractal, double new_zoom, int x, int y);
void		ft_move_d(t_fractal *fractal, int dx, int dy);
void		ft_move2center(t_fractal *fractal, int x, int y);
void		ft_exec_kernel(t_scene *p_scene);
void		ft_reset_bounds(t_fractal *fractal);
void		ft_reset_color(t_fractal *fractal);
char		*get_path(t_ftype ftype);

#endif
