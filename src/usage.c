/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:13:01 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:13:01 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "fractol.h"

static void	print_help(char *argv0)
{
	ft_printf("Usage:\n\t%s -[h] fractal_type\n", argv0);
	ft_printf("\t\t-h, --help\t\t show this help message.\n");
	ft_printf("\n\t\tfractal types:\n");
	ft_printf("\t\t\t- mandelbrot(m)\n");
	ft_printf("\t\t\t- julia(j)\n");
	ft_printf("\t\t\t- b-ship(b)\n");
}

void		ft_usage(char *argv0)
{
	print_help(argv0);
	exit(0);
}

void		ft_usage_delmlx(char *argv0, t_mlx **mlx)
{
	print_help(argv0);
	ft_mlxdel(mlx);
	exit(0);
}
