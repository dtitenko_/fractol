/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:13:09 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:13:10 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_FRACTOL_H
# define FRACTOL_FRACTOL_H

# include <stdlib.h>
# include <stdio.h>
# include <math.h>
# include <errno.h>

# include "mlx.h"

# ifndef MLX_INT_H
#  define MLX_INT_H
#  include "mlx_int.h"
# endif

/*
** MLX Window params
*/

# ifdef __APPLE__
#  include <OpenCL/opencl.h>
# else
#  include <CL/cl.h>
# endif

# define CLEARCOLOR 0x0

/*
** Flags for argv parsing
*/
# define F_HELP 0x01

# include "structs.h"
# include "hndlrs.h"
# include "scene.h"
# include "hooks.h"
# include "fractal.h"

# define SHOWHELP		0x01
# define SHOWINFO		0x02

/*
** mlx_init.c
*/
t_mlx			*ft_init_mlx(int numwin, t_ftype *types);
void			ft_mlxdel(t_mlx **mlx);
void			ft_init_mlx_hooks(t_mlx *mlx);
t_win_list		*ft_init_winlist(t_mlx *mlx);
t_scene			*ft_init_scenes(t_mlx *mlx);

/*
** error.c
*/
void			ft_die(int argc, char **argv, int code, t_mlx **mlx);

/*
** image.c
*/
void			img_put_pixel(t_img *image, int x, int y, int color);
void			clear_img(t_img *image, int color);

/*
** usage.c
*/
void			ft_usage(char *argv0);
void			ft_usage_delmlx(char *argv0, t_mlx **mlx);

/*
** argv_parsing.c
*/
int				parse_argv(int argc, char **argv, int *stop);
t_ftype			*ft_parse_fractals(int argc, char **argv, int *stop,
									t_mlx **mlx);

/*
** ft_init_kernel.c
*/
t_ocl			*ft_init_kernel(char *path, char *name);
void			draw_usage(t_mlx *mlx);

#endif
