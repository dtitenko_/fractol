/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:13:18 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:13:18 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_HOOKS_H
# define FRACTOL_HOOKS_H

# include "fractol.h"

/*
** kb_hooks.c
*/
int		keyrelease_hook(int keycode, t_mlx *mlx);
int		keypress_hook(int keycode, t_mlx *mlx);

/*
** mouse_hooks.c
*/
int		mouse_motion_hook(int x, int y, t_mlx *mlx);
int		mousebtn_press_hook(int button, int x, int y, t_mlx *mlx);
int		mousebtn_release_hook(int button, int x, int y, t_mlx *mlx);

/*
** other_hooks.c
*/
int		update(t_mlx *mlx);
int		exit_hook(t_mlx *mlx);

#endif
