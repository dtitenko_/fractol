/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   modifiers_hndlrs.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:45 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:48 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	modifiers_keypress(int keycode, t_mlx *mlx)
{
	mlx->keymodif |= (keycode == XK_Shift_L || keycode == XK_Shift_R) ?
					SHIFTMOD : 0x0;
	mlx->keymodif |= (keycode == XK_Control_L || keycode == XK_Control_R) ?
					CTRLMOD : 0x0;
	mlx->keymodif |= (keycode == XK_Alt_L || keycode == XK_Alt_R) ?
					ALTMOD : 0x0;
}

void	modifiers_keyrelease(int keycode, t_mlx *mlx)
{
	(keycode == XK_Shift_L || keycode == XK_Shift_R) ?
		mlx->keymodif &= ~SHIFTMOD : 0;
	(keycode == XK_Control_L || keycode == XK_Control_R) ?
		mlx->keymodif &= ~CTRLMOD : 0;
	(keycode == XK_Alt_L || keycode == XK_Alt_R) ?
		mlx->keymodif &= ~ALTMOD : 0;
}
