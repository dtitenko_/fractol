/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_zoom_fractal.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:26 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:29 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <include/fractol.h>

double		interpolate(double start, double end, double interpolation)
{
	return (start + ((end - start) * interpolation));
}

void		ft_count_factor(t_fractal *fractal)
{
	fractal->factor.re = (fractal->max.re - fractal->min.re) / (WINW);
	fractal->factor.im = (fractal->max.im - fractal->min.im) / (WINH);
}

void		ft_count_bounds(t_fractal *fractal, t_complex d)
{
	double		inter;
	t_complex	min;
	t_complex	max;

	inter = 1.0 / fractal->zoom;
	min.re = interpolate(d.re, fractal->min.re, inter);
	max.re = interpolate(d.re, fractal->max.re, inter);
	min.im = interpolate(d.im, fractal->min.im, inter);
	max.im = interpolate(d.im, fractal->max.im, inter);
	if (max.re - min.re == fractal->dim.re)
		return ;
	if (max.im - min.im == fractal->dim.im)
		return ;
	fractal->max = max;
	fractal->min = min;
	ft_count_factor(fractal);
	fractal->dim.im = max.im - min.im;
	fractal->dim.re = max.re - min.re;
}

void		ft_set_zoom(t_fractal *fractal, double new_zoom, int x, int y)
{
	t_complex	d;

	fractal->zoom = new_zoom;
	d = ft_recount_coordinates(fractal, x, y);
	ft_count_bounds(fractal, d);
}
