/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   image.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:33 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:34 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft/includes/libft.h>
#include "fractol.h"

void	img_put_pixel(t_img *image, int x, int y, int color)
{
	if (x < 0 || x >= image->width || y < 0 || y >= image->height)
		return ;
	*(int *)(image->data +
			(x + y * image->width) * image->bpp / 8) = color;
}

void	clear_img(t_img *image, int color)
{
	ft_memset(image->data, color,
			(size_t)image->width * image->height * image->bpp / 8);
}
