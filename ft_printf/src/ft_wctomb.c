/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wctomb.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 11:52:43 by exam              #+#    #+#             */
/*   Updated: 2016/12/29 14:59:56 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <wchar.h>

static int	conv_to_multibyte(int bytes, wchar_t chr, char *str)
{
	int		offset;

	offset = 0;
	while (++offset < bytes)
		str[offset] = 0x80 | (chr >> ((bytes - offset - 1) * 6) & 0x3F);
	--offset;
	if (bytes == 1)
		str[0] = (unsigned char)chr;
	else if (bytes == 2)
		str[0] = 0xC0 | (chr >> (offset * 6) & 0x1F);
	else if (bytes == 3)
		str[0] = 0xE0 | (chr >> (offset * 6) & 0xF);
	else if (bytes == 4)
		str[0] = 0xF0 | (chr >> (offset * 6) & 0x7);
	return (bytes);
}

int			ft_wctomb(char *str, wchar_t chr)
{
	int		i;
	int		count;

	if ((chr >= 0 && chr <= 0xD7FF) || (chr >= 0xE000 && chr <= 0x10FFFF))
	{
		count = 1;
		i = 7;
		while (i < 22)
		{
			if (chr >> i == 0)
				return (conv_to_multibyte(count, chr, str));
			i += 4 + (i > 7);
			++count;
		}
	}
	return (-1);
}
