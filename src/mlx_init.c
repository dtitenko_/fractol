/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_init.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:42 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:42 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft/includes/libft.h>
#include "include/fractol.h"
#include "include/fractal.h"
#include "hooks.h"

t_mlx		*ft_init_mlx(int numwin, t_ftype *types)
{
	t_mlx *mlx;

	if (!(mlx = (t_mlx *)malloc(sizeof(t_mlx))))
		exit(ENOMEM);
	mlx->winp = NULL;
	mlx->curr = NULL;
	mlx->keymodif = 0;
	mlx->flags = SHOWHELP;
	mlx->numscenes = numwin;
	mlx->types = types;
	if (!(mlx->mlxp = mlx_init()))
		return (NULL);
	mlx->mlxp->win_list = ft_init_winlist(mlx);
	ft_init_scenes(mlx);
	mlx_do_key_autorepeaton(mlx->mlxp);
	ft_init_mlx_hooks(mlx);
	return (mlx);
}

t_win_list	*ft_init_winlist(t_mlx *mlx)
{
	int			i;
	t_win_list	*head;
	t_win_list	*curr;
	char		*name;

	i = mlx->numscenes;
	if (i < 1)
		return (NULL);
	name = ft_itoa(i);
	head = mlx_new_window(mlx->mlxp, WINW, WINH, name);
	ft_strdel(&name);
	curr = head;
	while (--i)
	{
		name = ft_itoa(i);
		curr->next = mlx_new_window(mlx->mlxp, WINW, WINH, name);
		ft_strdel(&name);
		if (!curr->next)
			return (NULL);
		curr = curr->next;
		curr->next = NULL;
	}
	mlx->winp = head;
	return (head);
}

t_scene		*ft_init_scenes(t_mlx *mlx)
{
	t_win_list	*currwin;
	t_scene		*scenes;
	int			num;

	num = mlx->numscenes;
	if (!(scenes = (t_scene *)malloc(sizeof(t_scene) * num)))
		return (NULL);
	currwin = mlx->winp;
	while (num-- && currwin)
	{
		if (!((scenes[num].fractal = ft_init_fractal(mlx->types[num],
													&(scenes[num]))) &&
			(scenes[num].imgp = mlx_new_image(mlx->mlxp, WINW, WINH))))
		{
			ft_mlxdel(&mlx);
			return (NULL);
		}
		clear_img(scenes[num].imgp, 0);
		scenes[num].winp = currwin;
		currwin = currwin->next;
	}
	mlx->scenes = scenes;
	mlx->currscene = scenes[0];
	return (scenes);
}

void		ft_mlxdel(t_mlx **mlx)
{
	t_win_list	*next;
	int			num;

	if (mlx && *mlx)
	{
		while ((*mlx)->winp)
		{
			next = (*mlx)->winp->next;
			mlx_destroy_window((*mlx)->mlxp, (*mlx)->winp);
			(*mlx)->winp = next;
		}
		num = (*mlx)->numscenes;
		while (num--)
		{
			ftocl_free_ocl(&((*mlx)->scenes[num].p_ocl));
		}
		ft_memdel((void **)&((*mlx)->scenes));
		ft_memdel((void **)mlx);
	}
}

void		ft_init_mlx_hooks(t_mlx *mlx)
{
	t_win_list *wincurr;

	wincurr = mlx->winp;
	while (wincurr)
	{
		mlx_hook(wincurr, DestroyNotify, StructureNotifyMask |
										SubstructureNotifyMask, exit_hook, mlx);
		mlx_hook(wincurr, KeyRelease, KeyReleaseMask, keyrelease_hook, mlx);
		mlx_hook(wincurr, KeyPress, KeyPressMask, keypress_hook, mlx);
		mlx_hook(wincurr, ButtonPress, ButtonPressMask,
				mousebtn_press_hook, mlx);
		mlx_hook(wincurr, ButtonRelease, ButtonReleaseMask,
				mousebtn_release_hook, mlx);
		mlx_hook(wincurr, MotionNotify, PointerMotionMask,
				mouse_motion_hook, mlx);
		wincurr = wincurr->next;
	}
}
