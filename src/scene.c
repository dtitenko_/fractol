/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:57 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:57 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scene.h"
#include "fractol.h"

t_scene	*map_scenes(t_mlx *mlx, t_scene (*func)())
{
	int i;

	i = mlx->numscenes;
	while (i--)
	{
		mlx->scenes[i] = func(mlx->scenes[i], mlx);
	}
	return (mlx->scenes);
}

t_scene	clear_scene(t_scene scene, t_mlx *mlx)
{
	mlx_clear_window(mlx->mlxp, scene.winp);
	clear_img(scene.imgp, CLEARCOLOR);
	return (scene);
}

t_scene	draw_scene(t_scene scene, t_mlx *mlx)
{
	mlx_put_image_to_window(mlx->mlxp, scene.winp, scene.imgp, 0, 0);
	return (scene);
}

t_scene	update_scene(t_scene scene, t_mlx *mlx)
{
	scene.fractal->func(&scene);
	scene = draw_scene(scene, mlx);
	draw_usage(mlx);
	return (scene);
}

t_scene	find_scene(t_mlx *mlx, t_win_list *win)
{
	t_win_list	*curr;
	int			i;

	i = 0;
	curr = win;
	while (curr)
	{
		if (mlx->curr->window == mlx->scenes[i].winp->window)
			return (mlx->scenes[i]);
		i++;
		curr = curr->next;
	}
	return (mlx->scenes[0]);
}
