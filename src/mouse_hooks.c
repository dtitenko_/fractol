/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_hooks.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:51 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:51 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		mouse_motion_hook(int x, int y, t_mlx *mlx)
{
	t_scene scene;

	scene = mlx->currscene;
	if (!(scene.winp) || scene.winp->window != mlx->curr->window)
		scene = find_scene(mlx, mlx->winp);
	if (scene.fractal->type != julia)
		return (0);
	if (!scene.fractal->fixed)
	{
		scene.fractal->shift = ft_recount_coordinates(scene.fractal, x, y);
		update_scene(scene, mlx);
	}
	return (0);
}

int		mousebtn_press_hook(int button, int x, int y, t_mlx *mlx)
{
	t_scene scene;

	scene = find_scene(mlx, mlx->winp);
	if (button == 1)
	{
		ft_move2center(scene.fractal, x, y);
		update_scene(scene, mlx);
	}
	if (button == 4)
	{
		ft_set_zoom(scene.fractal, 1.03, x, y);
		update_scene(scene, mlx);
	}
	else if (button == 5)
	{
		ft_set_zoom(scene.fractal, 0.97, x, y);
		update_scene(scene, mlx);
	}
	return (0);
}

int		mousebtn_release_hook(int button, int x, int y, t_mlx *mlx)
{
	(void)x;
	(void)y;
	(void)mlx;
	(void)button;
	return (0);
}
