/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cl_kernal_from_file.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:09:25 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:11:57 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include "libft/includes/libft.h"
#include "include/fractol.h"
#include "include/ft_ocl.h"
#include "ft_printf/includes/ft_printf.h"

char		*read_all_from_fd(int fd)
{
	char	*line;
	char	*tmp;
	int		ret;
	char	*src;

	src = ft_strnew(0);
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		if (ret == -1)
		{
			ft_strdel(&line);
			ft_strdel(&tmp);
			ft_strdel(&src);
			return (NULL);
		}
		tmp = ft_strjoin(src, line);
		ft_strdel(&line);
		ft_strdel(&src);
		src = tmp;
	}
	ft_strdel(&line);
	return (src);
}

char		*cl_get_progrm_source(const char *filename)
{
	int		fd;
	char	*src;

	fd = open(filename, O_RDONLY);
	if (fd < 0)
		return (NULL);
	src = read_all_from_fd(fd);
	close(fd);
	return (src);
}

cl_kernel	ftocl_get_kernel_from_file(t_ocl *p_ocl, const char *filename,
										const char *name)
{
	int			err;
	char		*src;

	if (!(src = cl_get_progrm_source(filename)))
		return ((cl_kernel)0);
	OCL_PROGRAM = clCreateProgramWithSource(OCL_CTX, 1, (const char **)&src,
										NULL, &err);
	if (err != CL_SUCCESS)
		ft_printf("error : OpenCL : clCreateProgramWithSource : %d\n", err);
	ft_strdel(&src);
	err = clBuildProgram(OCL_PROGRAM, 0, NULL, NULL, NULL, NULL);
	if (err != CL_SUCCESS)
		ft_printf("error : OpenCL : clBuildProgram : %d\n", err);
	OCL_KRNL = clCreateKernel(OCL_PROGRAM, name, &err);
	if (err != CL_SUCCESS)
		ft_printf("error : OpenCL : clCreateKernel : %d\n", err);
	return (OCL_KRNL);
}

cl_context	ftocl_create_context(t_ocl *p_ocl)
{
	cl_int			err;

	if (!(OCL_DEV_IDS = malloc(sizeof(cl_device_id) * OCL_DEV_NUM)))
		return (NULL);
	err = clGetDeviceIDs(NULL, CL_DEVICE_TYPE_GPU, OCL_DEV_NUM, OCL_DEV_IDS,
						&OCL_DEV_NUM);
	if (err != CL_SUCCESS)
		ft_printf("error : OpenCL : clGetDeviceIDs : %d\n", err);
	OCL_CTX = clCreateContext(0, OCL_DEV_NUM, OCL_DEV_IDS, NULL, NULL, &err);
	if (err != CL_SUCCESS)
		ft_printf("error : OpenCL : clCreateContext : %d\n", err);
	return (OCL_CTX);
}
