/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ocl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:13:13 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:13:13 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_FT_OCL_H
# define FRACTOL_FT_OCL_H

# ifdef __APPLE__
#  include <OpenCL/opencl.h>
# else
#  define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#  include <CL/cl.h>
# endif

typedef struct			s_ocl_buf
{
	cl_mem				buf;
	size_t				buf_size;
	void				*mem;
}						t_ocl_buf;

typedef struct			s_ocl
{
	cl_context			ctx;
	cl_kernel			kernel;
	cl_program			program;
	cl_device_id		*dev_ids;
	cl_command_queue	*cmd_queues;
	t_ocl_buf			*bufs;
	cl_uint				buf_num;
	cl_uint				dev_type;
	cl_uint				dev_num;
	cl_int				err;
}						t_ocl;

# define OCL_CTX (p_ocl->ctx)
# define OCL_KRNL (p_ocl->kernel)
# define OCL_PROGRAM (p_ocl->program)
# define OCL_DEV_IDS (p_ocl->dev_ids)
# define OCL_DEV_TYPE (p_ocl->dev_type)
# define OCL_DEV_NUM (p_ocl->dev_num)
# define OCL_BUFS (p_ocl->bufs)
# define OCL_BUF_NUM (p_ocl->buf_num)
# define OCL_CMD_QUEUES (p_ocl->cmd_queues)
# define OCL_ERR (p_ocl->err)

cl_kernel				ftocl_get_kernel_from_file(t_ocl *p_ocl,
													const char *filename,
													const char *name);
cl_context				ftocl_create_context(t_ocl *p_ocl);
t_ocl					*ftocl_init_ocl(int dev_num, size_t *buf_sizes,
										cl_uint num_bufs);
void					ftocl_release_devices(t_ocl *p_ocl);
void					ftocl_release_mem_objs(t_ocl *p_ocl);
void					ftocl_free_ocl(t_ocl **pp_ocl);

#endif
