/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cl_release_all.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:11:59 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:00 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/ft_ocl.h"
#include "libft/includes/libft.h"

void	ftocl_release_mem_objs(t_ocl *p_ocl)
{
	cl_uint	i;

	i = 0;
	while (i++ < OCL_BUF_NUM)
		clReleaseMemObject(OCL_BUFS[i - 1].buf);
}

void	ftocl_release_devices(t_ocl *p_ocl)
{
	cl_uint i;

	i = 0;
	while (i++ < OCL_DEV_NUM)
	{
		clReleaseCommandQueue(OCL_CMD_QUEUES[i - 1]);
		clReleaseDevice(OCL_DEV_IDS[i - 1]);
	}
	ft_memdel((void **)&OCL_CMD_QUEUES);
	ft_memdel((void **)&OCL_DEV_IDS);
}

void	ftocl_free_ocl(t_ocl **pp_ocl)
{
	t_ocl *p_ocl;

	p_ocl = (*pp_ocl);
	ftocl_release_mem_objs(p_ocl);
	ftocl_release_devices(p_ocl);
	clReleaseProgram(OCL_PROGRAM);
	clReleaseKernel(OCL_KRNL);
	clReleaseContext(OCL_CTX);
	ft_memdel((void **)pp_ocl);
}
