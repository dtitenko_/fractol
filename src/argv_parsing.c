/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   argv_parsing.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:04 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:05 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fractol.h"

t_ftype			*ft_parse_fractals(int argc, char **argv, int *stop,
									t_mlx **mlx)
{
	t_ftype		*types;
	int			i;
	int			j;

	if (!(types = (t_ftype *)malloc(sizeof(t_ftype) * argc - *stop)))
		return (NULL);
	i = *stop - 1;
	j = -1;
	while (++i < argc)
	{
		j++;
		types[j] = (ft_strequ("mandelbrot", argv[i]) || ft_strequ("m", argv[i]))
				? mandelbrot : notype;
		types[j] = (ft_strequ("julia", argv[i]) || ft_strequ("j", argv[i]))
				? julia : types[j];
		types[j] = (ft_strequ("b-ship", argv[i]) || ft_strequ("b", argv[i]))
				? bship : types[j];
		if (types[j] == notype)
			ft_usage_delmlx(argv[0], mlx);
	}
	*stop = argc - *stop;
	return (types);
}

int				ft_parse_argv_flag_short(char *argvi)
{
	int		flag;

	flag = 0;
	argvi++;
	while (*argvi && ft_strchr("h", *argvi))
	{
		flag |= (*argvi == 'h') ? F_HELP : flag;
		argvi++;
	}
	flag |= (*argvi) ? F_HELP : flag;
	return (flag);
}

int				ft_parse_argv_flag_long(char *argvi)
{
	int	flag;

	flag = 0;
	flag |= (ft_strequ("--help", argvi)) ? F_HELP : flag;
	return (flag);
}

int				parse_argv(int argc, char **argv, int *stop)
{
	int	flags;
	int	i;

	flags = 0;
	i = 0;
	if (argc == 1)
		flags |= F_HELP;
	while (++i < argc)
		if (argv[i][0] == '-' && ft_isalpha(argv[i][1]))
			flags |= ft_parse_argv_flag_short(argv[i]);
		else if (ft_strnequ("--", argv[i], 2) && ft_isalpha(argv[i][3]))
			flags |= ft_parse_argv_flag_long(argv[i]);
		else
			break ;
	(stop) ? *stop = i : 0;
	if (flags & F_HELP)
		ft_usage(argv[0]);
	return (flags);
}
