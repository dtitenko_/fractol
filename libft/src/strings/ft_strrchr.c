/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 18:24:23 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/30 16:33:07 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*ss;

	ss = NULL;
	if (0 == c)
		return ((char *)(s + ft_strlen(s)));
	while (*s)
	{
		if (*s == c)
			ss = (char *)s;
		s++;
	}
	return ((char *)ss);
}
