double2 multComplex(double2 z, double2 c)
{
	double2 t;
	t.x = z.x * c.x - z.y * c.y;
	t.y = z.x * c.y + z.y * c.x;
	return t;
}

double2 powComplex(double2 z, int i)
{
	double2 t = z;
	for (int j = 0; j < i - 1; j++)
		t = multComplex(t, z);
	return t;
}

int		rgb2int(int r, int g, int b)
{
	r = (r << 16) & 0xFF0000;
	g = (g << 8) & 0xFF00;
	b = (b << 0) & 0xFF;
	return (r | g | b);
}

__kernel void render(__global int *image, __constant double *args)
{
	int				dim_x;
	int				dim_y;
	int				size_x;
	int				size_y;
	double2			z;
	double2			c;
	double2			z2;
	int				i;
	double			tmp;
	unsigned char	color1;

	size_x = get_global_size(0);
	size_y = get_global_size(1);
	dim_x = get_global_id(0);
	dim_y = get_global_id(1);
	i = -1;
	z.x = dim_x * args[7] + args[3];
	z.y = dim_y * args[8] + args[5];
	c.x = args[9];
	c.y = args[10];
	z2 = z * z;
	while(z2.x + z2.y <= 4 && ++i < args[0])
	{
		z = powComplex(z, args[12]);
		z = z + c;
		z2 = z * z;
	}
	color1 = args[1];
	if (i++ != args[0])
		image[(dim_x + dim_y * size_x)] = args[11] ?
		rgb2int(args[1] * i * 5, args[1] * i * 2, 0) :
		rgb2int(args[1] * i, args[1] * i, args[1] * i);
	else
		image[(dim_x + dim_y * size_x)] = 0;
}