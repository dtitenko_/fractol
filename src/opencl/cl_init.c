/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cl_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:09:19 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:09:21 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ocl.h"
#include "libft/includes/libft.h"

t_ocl	*ftocl_init_ocl(int dev_num, size_t *buf_sizes, cl_uint num_bufs)
{
	t_ocl	*p_ocl;
	int		i;

	if (!(p_ocl = ft_memalloc(sizeof(t_ocl))))
		return (NULL);
	p_ocl->dev_num = (cl_uint)dev_num;
	OCL_CTX = ftocl_create_context(p_ocl);
	i = -1;
	OCL_CMD_QUEUES = ft_memalloc(OCL_DEV_NUM);
	while (++i < (int)OCL_DEV_NUM)
		OCL_CMD_QUEUES[i] = clCreateCommandQueue(OCL_CTX, OCL_DEV_IDS[i],
												0, &OCL_ERR);
	OCL_BUF_NUM = num_bufs;
	if (!(OCL_BUFS = ft_memalloc(num_bufs * sizeof(t_ocl_buf))))
		return (NULL);
	while (num_bufs--)
		OCL_BUFS[num_bufs].buf_size = buf_sizes[num_bufs];
	return (p_ocl);
}
