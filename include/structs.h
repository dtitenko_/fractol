/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:13:28 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:13:28 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_STRUCTS_H
# define FRACTOL_STRUCTS_H

# include "ft_ocl.h"

typedef enum		e_ftype
{
	notype = 0x00,
	mandelbrot = 0x01,
	julia = 0x02,
	bship = 0x04
}					t_ftype;

typedef struct		s_complex
{
	double			re;
	double			im;
}					t_complex;

typedef struct		s_fractal
{
	t_ftype			type;
	int				fixed;
	int				maxiter;
	double			zoom;
	t_complex		min;
	t_complex		max;
	t_complex		shift;
	t_complex		factor;
	t_complex		dim;
	t_complex		tmp;
	t_complex		z;
	t_complex		c;
	int				pow;
	unsigned char	base_color1;
	unsigned char	base_color2;
	int				psychomod;
	void			(*func)();
}					t_fractal;

typedef struct		s_scene
{
	t_fractal		*fractal;
	t_win_list		*winp;
	t_img			*imgp;
	t_ocl			*p_ocl;
}					t_scene;

typedef struct		s_mlx
{
	t_xvar			*mlxp;
	t_win_list		*winp;
	t_win_list		*curr;
	t_scene			*scenes;
	t_ftype			*types;
	t_scene			currscene;
	int				numscenes;
	long			flags;
	int				keymodif;
}					t_mlx;

#endif
