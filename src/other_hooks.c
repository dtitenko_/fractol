/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   other_hooks.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:12:53 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:12:53 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "scene.h"

int		update(t_mlx *mlx)
{
	map_scenes(mlx, update_scene);
	return (0);
}

int		exit_hook(t_mlx *mlx)
{
	ft_mlxdel(&mlx);
	exit(0);
	return (0);
}
