/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   kb_release_hooks.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/24 19:29:15 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/24 19:29:16 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/fractol.h"

int		keyrelease_hook(int keycode, t_mlx *mlx)
{
	if (XK_F1 == keycode)
	{
		mlx->flags = (mlx->flags & SHOWHELP) ?
					mlx->flags & (~SHOWHELP) : mlx->flags | SHOWHELP;
		update(mlx);
	}
	else if (XK_F2 == keycode)
	{
		mlx->flags = (mlx->flags & SHOWINFO) ?
					mlx->flags & (~SHOWINFO) : mlx->flags | SHOWINFO;
		update(mlx);
	}
	return (0);
}
