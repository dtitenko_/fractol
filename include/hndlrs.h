/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hndlrs.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:13:15 by dtitenko          #+#    #+#             */
/*   Updated: 2017/09/23 20:13:15 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_HNDLRS_H
# define FRACTOL_HNDLRS_H

/*
** keys-modifiers
*/
# define CTRLMOD	0x01
# define ALTMOD		0x02
# define SHIFTMOD	0x04

/*
** handlers for keys-modifiers
*/
void modifiers_keypress(int keycode, t_mlx *mlx);
void modifiers_keyrelease(int keycode, t_mlx *mlx);

#endif
